package com.odoo.dpr.customshare.share;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.odoo.dpr.customshare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dharmang Soni on 29/6/16.
 */
public class ContentShareDialog implements AdapterView.OnItemClickListener {

    private Context mContext;
    private OnShareAppSelectListener mOnShareAppSelectListener;
    private Dialog dialog;
    private ListView listView;
    private List<AppInfo> applications = new ArrayList<>();
    private Intent requestIntent;

    public ContentShareDialog(@NonNull Context context) {
        mContext = context;
        listView = new ListView(mContext);
    }

    public void show(Intent requestIntent, OnShareAppSelectListener callback) {
        this.requestIntent = requestIntent;
        mOnShareAppSelectListener = callback;
        PackageManager pm = mContext.getPackageManager();
        List<ResolveInfo> apps = pm.queryIntentActivities(requestIntent, 0);
        for (ResolveInfo info : apps) {
            ActivityInfo activityInfo = info.activityInfo;
            applications.add(new AppInfo(info.loadLabel(pm).toString(), activityInfo.name,
                    new ComponentName(activityInfo.applicationInfo.packageName,
                            activityInfo.name), info.loadIcon(pm)));
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Share with...");
        ArrayAdapter<AppInfo> adapter = new ArrayAdapter<AppInfo>(mContext,
                R.layout.share_item_view, applications) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(mContext)
                            .inflate(R.layout.share_item_view, parent, false);
                }
                ImageView icon = (ImageView) convertView.findViewById(android.R.id.icon);
                icon.setImageDrawable(getItem(position).getIcon());
                TextView title = (TextView) convertView.findViewById(android.R.id.title);
                title.setText(getItem(position).getAppName());
                return convertView;
            }
        };
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        builder.setView(listView);
        builder.setNegativeButton(android.R.string.cancel, null);
        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        AppInfo appInfo = applications.get(i);
        if (requestIntent != null) {
            requestIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            requestIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            requestIntent.setComponent(appInfo.getComponentName());
            mContext.startActivity(requestIntent);
        }
        if (mOnShareAppSelectListener != null) {
            mOnShareAppSelectListener.shareAppSelected(appInfo);
        }

        dialog.dismiss();
    }

    public interface OnShareAppSelectListener {
        void shareAppSelected(AppInfo appInfo);
    }

    public class AppInfo {
        private String appName, appPackage;
        private ComponentName componentName;
        private Drawable icon;

        public AppInfo(String appName, String appPackage, ComponentName componentInfo,
                       Drawable icon) {
            this.appName = appName;
            this.appPackage = appPackage;
            this.componentName = componentInfo;
            this.icon = icon;
        }


        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getAppPackage() {
            return appPackage;
        }

        public void setAppPackage(String appPackage) {
            this.appPackage = appPackage;
        }

        public ComponentName getComponentName() {
            return componentName;
        }

        public void setComponentName(ComponentName componentName) {
            this.componentName = componentName;
        }

        public Drawable getIcon() {
            return icon;
        }

        public void setIcon(Drawable icon) {
            this.icon = icon;
        }

        @Override
        public String toString() {
            return "AppInfo{" +
                    "appName='" + appName + '\'' +
                    ", appPackage='" + appPackage + '\'' +
                    ", componentName=" + componentName +
                    '}';
        }
    }
}
