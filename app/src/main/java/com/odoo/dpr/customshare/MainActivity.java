package com.odoo.dpr.customshare;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.odoo.dpr.customshare.share.ContentShareDialog;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create your request intent
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
        sendIntent.setType("text/plain");

        // Just create this dialog and call show, with intent parameter.
        // When user click on any application you will get that application information.
        ContentShareDialog dialog = new ContentShareDialog(this);
        dialog.show(sendIntent, new ContentShareDialog.OnShareAppSelectListener() {
            @Override
            public void shareAppSelected(ContentShareDialog.AppInfo appInfo) {
                Log.v(">>", "App name:" + appInfo.getAppName());
                Log.v(">>", "App Package: " + appInfo.getAppPackage());
            }
        });

    }
}
